﻿using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using NUnit.Framework;
using Castle.DynamicProxy;
using System.Collections.Generic;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Bases
{
    public class TestBase
    {

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ExtentReportHelpers.CreateReport();
        }

        [SetUp]
        public void Setup()
        {
            ExtentReportHelpers.AddTest();
            DriverFactory.CreateInstance();
            DriverFactory.INSTANCE.Navigate().GoToUrl(BuilderJson.ReturnParameterAppSettings("DEFAULT_APPLICATION_URL"));
        }

        [TearDown]
        public void TearDown()
        {
            ExtentReportHelpers.AddTestResult();
            DriverFactory.QuitInstace();
        }

        
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
             ExtentReportHelpers.GenerateReport();
        }
    }
}
