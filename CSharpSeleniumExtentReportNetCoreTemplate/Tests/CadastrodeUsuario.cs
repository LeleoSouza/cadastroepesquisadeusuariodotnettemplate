﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CadastrodeUsuario : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        LoginFlows loginFlows;
        ManagerDashboardPage managerDashboardPage;
        SystemUserPage systemUserPage;
        AddUserPage addUserPage;
        #endregion

        [Test]
        public void CadastroDeNovoUsuarioComSucesso()
        {
            loginPage = new LoginPage();
            loginFlows = new LoginFlows();
            managerDashboardPage = new ManagerDashboardPage();
            systemUserPage = new SystemUserPage();
            addUserPage = new AddUserPage();


            #region Parameters
            string usuario = "Admin";
            string senha = "admin123";
            string nomeDoFuncionario = "Admin A";
            string tipoDeUsuario = "Admin";
            string senhaDoNovoUsuario = "Fulano1234H";
            string nomeDoNovoUsuario = GeneralHelpers.ReturnStringWithRandomCharacters(6);

            #endregion

            loginFlows.EfetuarLogin(usuario, senha);

            managerDashboardPage.ClicarEmMenuDoAdministrador();

            systemUserPage.ClicarEmAdicionarUsuario();

            addUserPage.SelecionarTipodeUsuário(tipoDeUsuario);
            addUserPage.PreencherNomedoFuncionario(nomeDoFuncionario);
            addUserPage.PreencherNomeDoUsuario(nomeDoNovoUsuario);
            addUserPage.PreencherSenhaDoUsuario(senhaDoNovoUsuario);
            addUserPage.PreencherConfirmacaoDaSenhaDoNovoUsuario(senhaDoNovoUsuario);
            addUserPage.LerMensagemDaQualidadeDaSenha();
            addUserPage.ClicarEmSalvar();

            systemUserPage.InformarNomeDoUsuarioParaPesquisa(nomeDoNovoUsuario);
            systemUserPage.ClicarNoBotaoDeProcura();

            Assert.AreEqual(nomeDoNovoUsuario, systemUserPage.RetornarNomeDoNovoUsuario(nomeDoNovoUsuario));



        }

    }
}
