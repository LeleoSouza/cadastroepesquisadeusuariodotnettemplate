﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class SystemUserPage : PageBase
    {
        #region Mapping

        By addButton = By.Id("btnAdd");
        By searchField = By.Id("searchSystemUser_userName");
        By searchButton = By.Id("searchBtn");
        public By EncontraLocatorUsuarioCadastrado(string nomeUsuario)
        {
            By newUserTextArea = By.XPath("//*[contains(text(),'"+nomeUsuario+"')]");
            return newUserTextArea;
        }

        
        #endregion

        #region Actions

        public void ClicarEmAdicionarUsuario()
        {
            Click(addButton);
        }
        public void InformarNomeDoUsuarioParaPesquisa(string nomeUsuario)
        {
            SendKeys(searchField, nomeUsuario );
        }

            
                        
        public void ClicarNoBotaoDeProcura()
        {
            Click(searchButton);
        }
        public string RetornarNomeDoNovoUsuario(string nomeUsuario)
        {
            By locatorUsuario = EncontraLocatorUsuarioCadastrado(nomeUsuario);
            return GetText(locatorUsuario);
        }
        #endregion
    }
}