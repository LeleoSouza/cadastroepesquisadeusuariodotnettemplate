﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ManagerDashboardPage : PageBase
    {
        
        #region Mapping

        By adminButton = By.Id("menu_admin_viewAdminModule");
                       
        #endregion
        

        #region Actions

        public void ClicarEmMenuDoAdministrador()
        {
            Click(adminButton);
        }
        
        #endregion


    }
}
