using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{


    public class AddUserPage : PageBase
    {

        #region Mapping

        By userTypeField = By.Id("systemUser_userType");
        By employeeNameField = By.Id("systemUser_employeeName_empName");
        By userNameField = By.Id("systemUser_userName");
        By passwordField = By.Id("systemUser_password");
        By confirmPasswordField = By.Id("systemUser_confirmPassword");
        By saveButton = By.Id("btnSave");
        By passwordConfirmationTextArea = By.Id("systemUser_password_strength_meter");

        #endregion

        #region Actions

        public void SelecionarTipodeUsuário(string tipoDeUsuario)
        {
            ComboBoxSelectByVisibleText(userTypeField, tipoDeUsuario);    

        }
       
       public void PreencherNomedoFuncionario(string nomeDoFuncionario)
       {
           SendKeys(employeeNameField, nomeDoFuncionario); 

       }
        public void PreencherNomeDoUsuario(string nomeDoNovoUsuario)
        {
            
            SendKeys(userNameField, nomeDoNovoUsuario);
        }
        public void PreencherSenhaDoUsuario(string senhaDoNovoUsuario)
        {
            SendKeys(passwordField, senhaDoNovoUsuario);
        }
        public void PreencherConfirmacaoDaSenhaDoNovoUsuario(string senhaDoNovoUsuario)
        {
            SendKeys(confirmPasswordField,senhaDoNovoUsuario);
        }

        public void LerMensagemDaQualidadeDaSenha()
        {
            WaitForElement(passwordConfirmationTextArea);
        }
        public void ClicarEmSalvar()
        {
            Click(saveButton);
        }


        #endregion




        
    }
}